﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: Track.cs
// Description: 1. Contains Collection of Session. 
//              2. Provide function to convert collection to string.
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Properties;

    /// <summary>
    /// The Track
    /// </summary>
    public class Track
    {
        /// <summary>
        /// Gets or sets the session list.
        /// </summary>
        /// <value>
        /// The session list.
        /// </value>
        public ICollection<Session> SessionList { get; set; }

        /// <summary>
        /// Gets or sets the track number.
        /// </summary>
        /// <value>
        /// The track number.
        /// </value>
        public int TrackNumber { get; set; }

        /// <summary>
        /// Returns a Concatenated String of Sessions.
        /// </summary>
        /// <returns>
        /// The Resulted String.
        /// </returns>
        public override string ToString()
        {
            var resultStringbuilder = new StringBuilder();
            if (this.SessionList != null && this.SessionList.Count > 0)
            {
                resultStringbuilder.Append(Resources.LabelTrack);
                resultStringbuilder.Append(this.TrackNumber);
                resultStringbuilder.Append(System.Environment.NewLine);

                for (int index = 0; index < this.SessionList.Count; index++)
                {
                    resultStringbuilder.Append(this.SessionList.ElementAt(index).ToString());

                    // Check if break next
                    if (index < this.SessionList.Count - 1)
                    {
                        if (this.SessionList.ElementAt(index).EndTime != this.SessionList.ElementAt(index + 1).StartTime)
                        {
                            resultStringbuilder.Append(this.SessionList.ElementAt(index).EndTime.ToString("h:mmtt"));
                            resultStringbuilder.Append(" ");
                            resultStringbuilder.Append(Resources.LabelBreak);
                            resultStringbuilder.Append(System.Environment.NewLine);
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(ConferenceConfig.GetConfigInstance().LastEvent))
                {
                    resultStringbuilder.Append(ConferenceConfig.GetConfigInstance().LastEventMaxStartTime);
                    resultStringbuilder.Append(" ");
                    resultStringbuilder.Append(ConferenceConfig.GetConfigInstance().LastEvent);
                    resultStringbuilder.Append(System.Environment.NewLine);
                }
            }

            return resultStringbuilder.ToString();
        }
    }
}
