﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: ConsoleOutputWriter.cs
// Description: 1. Implements Output Writer Interface for Console
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;

    /// <summary>
    /// Write Output to Console
    /// </summary>
    class ConsoleOutputWriter : IOutputWriter
    {
        /// <summary>
        /// Writes the output.
        /// </summary>
        /// <param name="outputToWrite">The Output.</param>
        public void WriteOutput(string outputToWrite)
        {
            Console.WriteLine(outputToWrite.ToString());
        }
    }
}
