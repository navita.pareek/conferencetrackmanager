﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: IOutputWriter.cs
// Description: 1. Interface for Output Writer 
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    /// <summary>
    /// Output Writer Interface
    /// </summary>
    public interface IOutputWriter
    {
        /// <summary>
        /// Writes the output.
        /// </summary>
        /// <param name="outputToWrite">The Output.</param>
        void WriteOutput(string outputToWrite);
    }
}
