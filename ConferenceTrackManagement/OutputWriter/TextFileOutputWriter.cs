﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: TextFileOutputWriter.cs
// Description: 1. Implements Output Writer Interface for text file
//------------------------------------------------------------------------------

using System;

namespace ConferenceTrackManagement
{
    using System.IO;
    using Properties;
    using NLog;

    /// <summary>
    /// Output Writer for text file
    /// </summary>
    class TextFileOutputWriter : IOutputWriter
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Writes the output.
        /// </summary>
        /// <param name="outputToWrite">The Output.</param>
        public void WriteOutput(string outputToWrite)
        {
            string outputFilename = Resources.DefaultOutputTextFileName;

            if (!string.IsNullOrWhiteSpace(ConferenceConfig.GetConfigInstance().TracksOutputFilename))
                outputFilename = ConferenceConfig.GetConfigInstance().TracksOutputFilename;

            try
            {
                File.WriteAllText(outputFilename, outputToWrite.ToString());
            }
            catch(Exception exception)
            {
                logger.Error(Resources.ErrorMessageOutputWriterFailed);
                logger.Error(exception);
            }
        }
    }
}
