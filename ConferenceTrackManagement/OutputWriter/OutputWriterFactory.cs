﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: OutputWriterFactory.cs
// Description: Factory to provide output writer implementation. 
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using Properties;

    /// <summary>
    /// Output Writer Factory
    /// </summary>
    public class OutputWriterFactory
    {
        /// <summary>
        /// The output writer factory instance
        /// </summary>
        private static OutputWriterFactory _outputWriterFactoryInstance;

        /// <summary>
        /// The output writer instance
        /// </summary>
        private IOutputWriter _outputWriterInstance;

        /// <summary>
        /// Prevents a default instance of the <see cref="OutputWriterFactory"/> class from being created.
        /// </summary>
        private OutputWriterFactory()
        {
        }

        /// <summary>
        /// Gets the factory instance.
        /// </summary>
        /// <returns>The Factory Instance</returns>
        public static OutputWriterFactory GetFactoryInstance()
        {
            if (_outputWriterFactoryInstance == null)
                _outputWriterFactoryInstance = new OutputWriterFactory();

            return _outputWriterFactoryInstance;
        }

        /// <summary>
        /// Gets the output writer instance.
        /// Default is Console writer.
        /// </summary>
        /// <returns>The output writer</returns>
        public IOutputWriter GetOutputWriterInstance()
        {
            string outputFilename = ConferenceConfig.GetConfigInstance().TracksOutputFilename;

            if (outputFilename.EndsWith(Resources.ExtensionTextFile, StringComparison.OrdinalIgnoreCase))
            {
                _outputWriterInstance = new TextFileOutputWriter();
            }
            else
            {
                _outputWriterInstance = new ConsoleOutputWriter();
            }

            return _outputWriterInstance;
        }
    }
}
