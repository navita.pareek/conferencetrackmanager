﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: Session.cs
// Description: 1. Provide functions to clone and convert Session information to string.
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// The Session
    /// </summary>
    public class Session : ICloneable
    {
        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        /// <value>
        /// The start time.
        /// </value>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        /// <value>
        /// The end time.
        /// </value>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Gets or sets the talks list.
        /// </summary>
        /// <value>
        /// The talks list.
        /// </value>
        public ICollection<Talk> TalksList { get; set; }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Returns a Concatenated String which carries information of talks.
        /// </summary>
        /// <returns>
        /// The Resulted String.
        /// </returns>
        public override string ToString()
        {
            var resultStringbuilder = new StringBuilder();

            if (this.TalksList != null && this.TalksList.Count > 0)
            {
                var nextTalkStartTime = this.StartTime; 
                for (int index = 0; index < this.TalksList.Count; index++)
                {
                    resultStringbuilder.Append(this.TalksList.ElementAt(index).ToString(ref nextTalkStartTime));
                    resultStringbuilder.Append(Environment.NewLine);
                } 
            }

            return resultStringbuilder.ToString();
        }
    }
}
