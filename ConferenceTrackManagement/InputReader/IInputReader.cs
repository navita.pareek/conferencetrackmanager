﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: IInputReader.cs
// Description: 1. Interface for Input Reader 
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    /// <summary>
    /// Input Reader Interface
    /// </summary>
    public interface IInputReader
    {
        /// <summary>
        /// Reads the input.
        /// </summary>
        /// <returns>The string array of input</returns>
        string[] ReadInput();
    }
}
