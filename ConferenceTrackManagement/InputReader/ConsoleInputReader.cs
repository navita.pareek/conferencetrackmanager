﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: ConsoleInputReader.cs
// Description: 1. Implements Input Reader Interface for Console
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>
    /// Reads Input provided through Console
    /// </summary>
    public class ConsoleInputReader : IInputReader
    {
        /// <summary>
        /// Reads input.
        /// </summary>
        /// <returns>The array of string</returns>
        public string[] ReadInput()
        {
            var inputList = new List<string>();
            string input;

            do
            {
                input = Console.ReadLine();
                if (!string.IsNullOrEmpty(input))
                {
                    inputList.Add(input);
                }
            } while (!string.IsNullOrEmpty(input));
            
            return inputList.ToArray();
        }
    }
}
