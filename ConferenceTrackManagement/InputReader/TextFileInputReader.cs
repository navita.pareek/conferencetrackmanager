﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: TextFileInputReader.cs
// Description: 1. Implements Input Reader Interface for text file
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Properties;
    using NLog;

    /// <summary>
    /// Reads Input provided through text file
    /// </summary>
    public class TextFileInputReader : IInputReader
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Reads the input.
        /// </summary>
        /// <returns>
        /// The string array of input
        /// </returns>
        public string[] ReadInput()
        {
            string talksInputFilename = ConferenceConfig.GetConfigInstance().TalksInputFilename;
            if (!File.Exists(talksInputFilename))
            {
               talksInputFilename = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + talksInputFilename;

                if (!File.Exists(talksInputFilename))
                {
                    logger.Error(Resources.ErrorMessageTalkInputFileInvalid);
                    return null;
                }
            }

            var inputList = new List<string>();
            using (var streamReader = File.OpenText(talksInputFilename))
            {
                string input;
                do
                {
                    input = streamReader.ReadLine();
                    if (!string.IsNullOrEmpty(input))
                    {
                        inputList.Add(input);
                    }
                } while (!string.IsNullOrEmpty(input));
            }
            
            return inputList.ToArray();
        }
    }
}
