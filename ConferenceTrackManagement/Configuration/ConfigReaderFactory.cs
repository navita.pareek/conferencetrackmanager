﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: ConfigReaderFactory.cs
// Description: Factory to provide configuration elements updater. 
//------------------------------------------------------------------------------

using NLog.Fluent;

namespace ConferenceTrackManagement
{
    using System;
    using Properties;
    using NLog;

    /// <summary>
    /// Factory to provide configuration elements updater
    /// </summary>
    public class ConfigReaderFactory
    {
        /// <summary>
        /// The configuration updater factory instance
        /// </summary>
        private static ConfigReaderFactory _configReaderFactoryInstance;

        /// <summary>
        /// The configuration updater instance
        /// </summary>
        private IConfigReader _configReaderInstance;

        /// <summary>
        /// The logger
        /// </summary>
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Prevents a default instance of the <see cref="ConfigReaderFactory"/> class from being created.
        /// </summary>
        private ConfigReaderFactory()
        {
        }

        /// <summary>
        /// Gets the factory instance.
        /// </summary>
        /// <returns>The Factory</returns>
        public static ConfigReaderFactory GetFactoryInstance()
        {
            if (_configReaderFactoryInstance == null)
                _configReaderFactoryInstance = new ConfigReaderFactory();

            return _configReaderFactoryInstance;
        }

        /// <summary>
        /// Gets the input collector instance.
        /// </summary>
        /// <returns>The Updater</returns>
        /// <exception cref="System.NotImplementedException">To be Implemented by next release.</exception>
        public IConfigReader GetInputCollectorInstance()
        {
            string configFilename = Global.ConfigFilename;

            if (configFilename.EndsWith(Resources.ExtensionXmlFile, StringComparison.OrdinalIgnoreCase))
            {
                _configReaderInstance = new XmlConfigReader();
            }
            else
            {
                logger.Error(Resources.ErrorMessageUpdaterInvalid);
                throw new NotSupportedException(Resources.ErrorMessageUpdaterInvalid);
            }
            return _configReaderInstance;
        }
    }
}
