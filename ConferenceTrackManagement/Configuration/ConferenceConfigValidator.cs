﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: ConferenceConfigValidator.cs
// Description: Validate Configuration elements. 
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using System.Linq;
    using Properties;
    using NLog;

    /// <summary>
    /// Validates the configuration elements provider
    /// </summary>
    public class ConferenceConfigValidator
    {
        /// <summary>
        /// The conference configuration
        /// </summary>
        private ConferenceConfig _conferenceConfig;

        /// <summary>
        /// The logger
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Validates the configuration.
        /// </summary>
        /// <returns>The boolean status</returns>
        public bool ValidateConfig()
        {
            this._conferenceConfig = ConferenceConfig.GetConfigInstance();

            // Validates if sessions' information provided are not overlapping
            if (!this.ValidSessions())
            {
                logger.Error(Resources.ErrorMessageSessionsInvalid);
                return false;
            }

            // Validates if last event is exclusive
            if (!this.ValidLastEventMaxStartTime())
            {
                logger.Error(Resources.ErrorMessageLastEventInvalid);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Valids the sessions.
        /// </summary>
        /// <returns>The boolean status</returns>
        private bool ValidSessions()
        {
            if (!Global.Valid(_conferenceConfig.SessionsList))
                return false;

            DateTime prevSessionEndTime = default(DateTime);

            // Iterate through all sessions and check if all exclusive
            foreach(var session in _conferenceConfig.SessionsList)
            {
                if (session.EndTime == default(DateTime) || session.StartTime == default(DateTime) || session.StartTime > session.EndTime)
                    return false;

                if(prevSessionEndTime != default(DateTime) && prevSessionEndTime > session.StartTime)
                    return false;

                prevSessionEndTime = session.EndTime;
            }

            return true;
        }

        /// <summary>
        /// Valids the last event maximum start time.
        /// </summary>
        /// <returns>The boolean status</returns>
        private bool ValidLastEventMaxStartTime()
        {
            // Last event is not there
            if (string.IsNullOrWhiteSpace(_conferenceConfig.LastEvent))
            {
                return true;
            }

            var sessionEndTimeInList = _conferenceConfig.SessionsList.Last().EndTime;

            // If last event max time not provided,
            // provide pervious session end time as last events start time
            if (string.IsNullOrWhiteSpace(_conferenceConfig.LastEventMaxStartTime))
            {
                _conferenceConfig.LastEventMaxStartTime = sessionEndTimeInList.ToString("h:mmtt");
            }
            else
            {
                // Check if last event is exclusive
                var lastSessionMaxStartTime = Convert.ToDateTime(_conferenceConfig.LastEventMaxStartTime);
                if (sessionEndTimeInList < lastSessionMaxStartTime)
                    return false;
            }

            return true;
        }
    }
}
