﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: ConferenceConfig.cs
// Description: Singleton Class to Manage the Configuration elements. 
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Properties;

    /// <summary>
    /// The Conference Configurations
    /// </summary>
    public class ConferenceConfig
    {
        /// <summary>
        /// The sessions list
        /// </summary>
        public ICollection<Session> SessionsList { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [allow duplicate talks].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow duplicate talks]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowDuplicateTalks { get; set; } = false;

        /// <summary>
        /// The talk duration unit
        /// </summary>
        public string TalkDurationUnit { get; set; } = Resources.DefaultTalkDurationUnit;

        /// <summary>
        /// The lightning talk duration
        /// </summary>
        public string LightningTalkDuration { get; set; } = Resources.DefaultLightningTalkDuration;

        /// <summary>
        /// The talks input filename
        /// </summary>
        public string TalksInputFilename { get; set; }

        /// <summary>
        /// The tracks output filename
        /// </summary>
        public string TracksOutputFilename { get; set; }

        /// <summary>
        /// The default duration
        /// </summary>
        public string DefaultDuration { get; set; } = Resources.DefaultTalkDuration;

        /// <summary>
        /// The last event
        /// </summary>
        public string LastEvent { get; set; }

        /// <summary>
        /// The last event maximum start time
        /// </summary>
        public string LastEventMaxStartTime { get; set; }

        /// <summary>
        /// The conference configuration instance
        /// </summary>
        private static ConferenceConfig ConferenceConfigInstance { get; set; }

        /// <summary>
        /// Prevents a default instance of the <see cref="ConferenceConfig"/> class from being created.
        /// </summary>
        private ConferenceConfig()
        {
        }

        /// <summary>
        /// Gets the configuration instance.
        /// </summary>
        /// <returns>The Configuration Object</returns>
        public static ConferenceConfig GetConfigInstance()
        {
            if (ConferenceConfigInstance == null)
                ConferenceConfigInstance = new ConferenceConfig();

            return ConferenceConfigInstance;
        }

        /// <summary>
        /// Returns a Concatenated String which carries the configuration information.
        /// </summary>
        /// <returns>
        /// The Resulted String.
        /// </returns>
        public override string ToString()
        {
            var resultStringbuilder = new StringBuilder();

            resultStringbuilder.Append("Configuration Filename:");
            resultStringbuilder.Append(Global.ConfigFilename);
            resultStringbuilder.Append(Environment.NewLine);
            
            return resultStringbuilder.ToString();
        }
    }
}
