﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: IConfigReader.cs
// Description: Interface for reading the configuration file. 
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    /// <summary>
    /// Interface to Update configuration elements
    /// </summary>
    public interface IConfigReader
    {
        /// <summary>
        /// Reads the Conference Configuration.
        /// </summary>
        void ReadConferenceConfig();
    }
}
