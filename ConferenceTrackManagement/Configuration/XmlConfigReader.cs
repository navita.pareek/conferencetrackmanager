﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: XmlConfigReader.cs
// Description: Reads the Configuration elements by reading XML configuration file. 
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using Properties;
    using NLog;

    /// <summary>
    /// Reads the configuration elements
    /// </summary>
    public class XmlConfigReader : IConfigReader
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Gets or sets the conference configuration.
        /// </summary>
        /// <value>
        /// The conference configuration.
        /// </value>
        private ConferenceConfig conferenceConfig { get; set; }

        /// <summary>
        /// Gets or sets the configuration XML document.
        /// </summary>
        /// <value>
        /// The configuration XML document.
        /// </value>
        private XmlDocument configXmlDoc { get; set; }

        /// <summary>
        /// Populates the conference configuration.
        /// </summary>
        public void ReadConferenceConfig()
        {
            string fileName = Global.ConfigFilename;

            // Exit if configuration file not exists
            if (string.IsNullOrWhiteSpace(fileName))
            {
                logger.Error(Resources.ErrorMessageConfigFilenameInvalid);
                return;
            }

            if (!File.Exists(fileName))
            {
                fileName = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + fileName;
                if (!File.Exists(fileName))
                {
                    logger.Error(Resources.ErrorMessageConfigFilenameInvalid);
                    return;
                }
            }

            // Read Configuration file
            this.conferenceConfig = ConferenceConfig.GetConfigInstance();
            this.configXmlDoc = new XmlDocument();
            this.configXmlDoc.Load(fileName);

            if (this.configXmlDoc.DocumentElement != null)
            {
                this.ReadOutputFilename();
                this.ReadLastEventDetails();
                this.ReadSessionsDetails();
                this.ReadTalkDurationUnit();
                this.ReadTalksInputFilename();
                this.ReadTalkDefaultDuration();
                this.ReadLightningTalkDuration();
                this.ReadIfDuplicateTalkAllowed();
            }
            else
            {
                logger.Error(Resources.ErrorMessageConfigFileReadFailed);
            }
        }

        /// <summary>
        /// Reads if duplicate talk allowed.
        /// </summary>
        private void ReadIfDuplicateTalkAllowed()
        {
            XmlNode node = this.configXmlDoc.DocumentElement.SelectSingleNode(Resources.XPathTagTalk);

            if (node != null && node.Attributes != null)
            {
                string ifAllowDuplicateTalks = node.Attributes[Resources.XPathAttriuteAllowDuplicateTalk] != null ? node.Attributes[Resources.XPathAttriuteAllowDuplicateTalk].InnerText : null;

                if (!string.IsNullOrWhiteSpace(ifAllowDuplicateTalks) && (ifAllowDuplicateTalks.Equals("true", StringComparison.OrdinalIgnoreCase) || ifAllowDuplicateTalks.Equals("1")))
                    this.conferenceConfig.AllowDuplicateTalks = true;
            }
            else
            {
                logger.Debug(Resources.LogMessageAllowDuplicateAbsent);
                logger.Debug(Resources.LogMessageUsingDefaultValue + this.conferenceConfig.AllowDuplicateTalks);
            }
        }

        /// <summary>
        /// Reads the last event details.
        /// </summary>
        private void ReadLastEventDetails()
        {
            XmlNode node = this.configXmlDoc.DocumentElement.SelectSingleNode(Resources.XPathTagLastEvent);

            if (node != null && node.Attributes != null)
            {
                string lastEvent = node.Attributes[Resources.XPathAttributeLastEventDescrption] != null ? node.Attributes[Resources.XPathAttributeLastEventDescrption].InnerText : null;

                if (!string.IsNullOrWhiteSpace(lastEvent))
                    this.conferenceConfig.LastEvent = lastEvent;

                string lastEventMaxStartTime = node.Attributes[Resources.XPathAttributeLastEventMaxStartTime] != null ? node.Attributes[Resources.XPathAttributeLastEventMaxStartTime].InnerText : null;

                if (!string.IsNullOrWhiteSpace(lastEventMaxStartTime))
                    this.conferenceConfig.LastEventMaxStartTime = lastEventMaxStartTime;
            }
            else
            {
                logger.Debug(Resources.LogMessageLastEventAbsent);
            }
        }

        /// <summary>
        /// Reads the output filename.
        /// </summary>
        private void ReadOutputFilename()
        {
            XmlNode node = this.configXmlDoc.DocumentElement.SelectSingleNode(Resources.XPathTagTrack);

            if (node != null && node.Attributes != null)
            {
                string outputFilename = node.Attributes[Resources.XPatAttributeTrackOutputFile] != null
                    ? node.Attributes[Resources.XPatAttributeTrackOutputFile].InnerText
                    : null;

                if (!string.IsNullOrWhiteSpace(outputFilename))
                    this.conferenceConfig.TracksOutputFilename = outputFilename;
            }
            else
            {
                logger.Debug(Resources.LogMessageOutputFilenameAbsent);
            }
        }

        /// <summary>
        /// Reads the talk default duration.
        /// </summary>
        private void ReadTalkDefaultDuration()
        {
            XmlNode node = this.configXmlDoc.DocumentElement.SelectSingleNode(Resources.XPathTagTalk);

            if (node != null && node.Attributes != null)
            {
                string defaultDuration = node.Attributes[Resources.XPathAttributeDefaultDuration] != null ? node.Attributes[Resources.XPathAttributeDefaultDuration].InnerText : null;

                if (!string.IsNullOrWhiteSpace(defaultDuration))
                {
                    defaultDuration = defaultDuration.ToLower().Replace(this.conferenceConfig.TalkDurationUnit.ToLower(), string.Empty);
                    int intParseOutput;

                    if (int.TryParse(defaultDuration, out intParseOutput))
                    {
                        this.conferenceConfig.DefaultDuration = defaultDuration;
                    }
                    else
                    {
                        logger.Info(Resources.ErrorMessageDefaultDurationInvalid);
                        logger.Info(Resources.LogMessageUsingDefaultValue + this.conferenceConfig.DefaultDuration);
                    }
                }
            }
            else
            {
                logger.Debug(Resources.LogMessageDefaultDurationAbsent);
                logger.Debug(Resources.LogMessageUsingDefaultValue + this.conferenceConfig.DefaultDuration);
            }
        }

        /// <summary>
        /// Reads the talks input filename.
        /// </summary>
        private void ReadTalksInputFilename()
        {
            XmlNode node = this.configXmlDoc.DocumentElement.SelectSingleNode(Resources.XPathTagTalk);

            if (node != null && node.Attributes != null)
            {
                string talksFilename = node.Attributes[Resources.XPathAttributeInputTalkFilename] != null ? node.Attributes[Resources.XPathAttributeInputTalkFilename].InnerText : null;

                if (!string.IsNullOrWhiteSpace(talksFilename))
                    this.conferenceConfig.TalksInputFilename = talksFilename;
            }
            else
            {
                logger.Debug(Resources.LogMessageTalkInputFileAbsent);
            }
        }

        /// <summary>
        /// Reads the duration of the lightning talk.
        /// </summary>
        private void ReadLightningTalkDuration()
        {
            XmlNode node = this.configXmlDoc.DocumentElement.SelectSingleNode(Resources.XPathTagTalk);

            if (node != null && node.Attributes != null)
            {
                string lightningTalkDuration = node.Attributes[Resources.XPathAttributeLightningDuration] != null ? node.Attributes[Resources.XPathAttributeLightningDuration].InnerText : null;

                if (!string.IsNullOrWhiteSpace(lightningTalkDuration))
                {
                    lightningTalkDuration = lightningTalkDuration.ToLower().Replace(this.conferenceConfig.TalkDurationUnit.ToLower(), string.Empty);
                    int intParseOutput;

                    if (int.TryParse(lightningTalkDuration, out intParseOutput))
                    {
                        this.conferenceConfig.LightningTalkDuration = lightningTalkDuration;
                    }
                    else
                    {
                        logger.Info(Resources.ErrorMessageLightningDurationInvalid);
                        logger.Info(Resources.LogMessageUsingDefaultValue + this.conferenceConfig.LightningTalkDuration);
                    }
                }
            }
            else
            {
                logger.Debug(Resources.LogMessageLightningDurationAbsent);
                logger.Debug(Resources.LogMessageUsingDefaultValue + this.conferenceConfig.LightningTalkDuration);
            }
        }

        /// <summary>
        /// Reads the talk duration format.
        /// </summary>
        private void ReadTalkDurationUnit()
        {
            XmlNode node = this.configXmlDoc.DocumentElement.SelectSingleNode(Resources.XPathTagTalk);

            if (node != null && node.Attributes != null)
            {
                string durationTimeUnit = node.Attributes[Resources.XPathAttributeDurationUnit] != null ? node.Attributes[Resources.XPathAttributeDurationUnit].InnerText : null;

                if (!string.IsNullOrWhiteSpace(durationTimeUnit))
                    this.conferenceConfig.TalkDurationUnit = durationTimeUnit;
            }
            else
            {
                logger.Debug(Resources.LogMessageTalkDurationFormatAbsent);
                logger.Debug(Resources.LogMessageUsingDefaultValue + this.conferenceConfig.TalkDurationUnit);
            }
        }

        /// <summary>
        /// Reads the sessions details.
        /// </summary>
        private void ReadSessionsDetails()
        {
            XmlNodeList nodeList = this.configXmlDoc.DocumentElement.GetElementsByTagName(Resources.XPathTagSessions);

            if (nodeList != null && nodeList.Count > 0)
            {
                conferenceConfig.SessionsList = new List<Session>();
                var tempList = new List<Session>();

                // Update session list with start and end time provided.
                foreach (XmlNode node in nodeList)
                {
                    if (node.Attributes != null)
                    {
                        var session = new Session();

                        string startTime = node.Attributes[Resources.XPathAttributeSessionStartTime] != null ? node.Attributes[Resources.XPathAttributeSessionStartTime].InnerText : null;
                        if (!string.IsNullOrWhiteSpace(startTime) && ValidTime(startTime))
                            session.StartTime = Convert.ToDateTime(startTime);

                        string endTime = node.Attributes[Resources.XPathAttributeSessionEndTime] != null ? node.Attributes[Resources.XPathAttributeSessionEndTime].InnerText : null;
                        if (!string.IsNullOrWhiteSpace(endTime) && ValidTime(endTime))
                            session.EndTime = Convert.ToDateTime(endTime);

                        tempList.Add(session);
                    }
                    else
                    {
                        logger.Debug(Resources.LogMessageSesssionConfigAbsent);
                    }
                }

                // Save sessions in increasing start time.
                this.conferenceConfig.SessionsList = tempList.OrderBy(x => x.StartTime).ToList();
            }
            else
            {
                logger.Debug(Resources.LogMessageSesssionConfigAbsent);
            }
        }

        /// <summary>
        /// Valids the time.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns>The Boolean Status</returns>
        private bool ValidTime(string time)
        {
            DateTime tempTime;
            return DateTime.TryParse(time, out tempTime);
        }
    }
}
