﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: ConferenceScheduleGenerator.cs
// Description: 1. Generates the Conference Schedule from the talks list provided.
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Properties;
    using NLog;

    /// <summary>
    /// Tracks Manager
    /// </summary>
    public class ConferenceScheduleGenerator
    {
        /// <summary>
        /// The duration talk dictionary
        /// </summary>
        private Dictionary<int, List<Talk>> _durationTalkDictionary;

        /// <summary>
        /// The logger
        /// </summary>
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initializes a new instance of the <see cref="ConferenceScheduleGenerator"/> class.
        /// </summary>
        public ConferenceScheduleGenerator()
        {
            _durationTalkDictionary = new Dictionary<int, List<Talk>>();
        }

        public ConferenceSchedule GenerateConferenceSchedule(ICollection<Talk> talkList)
        {
            if (!Global.Valid(talkList))
            {
                logger.Error(Resources.ErrorMessageTalkListInvalid);
                return null;
            }

            if (!this.ValidSessionsList())
            {
                logger.Error(Resources.ErrorMessageSessionsInvalid);
                return null;
            }

            var conferenceSchedule = new ConferenceSchedule
            {
                TrackList = new List<Track>()
            };

            this.PopulateDurationTalkDictionary(talkList);
            var sessionList = ConferenceConfig.GetConfigInstance().SessionsList;
            var trackCount = 0;

            do
            {
                var newTrack = new Track
                {
                   SessionList = new List<Session>(),
                   TrackNumber = ++trackCount
                };

                // Create suitable talks list for each Session Configured
                foreach (var session in sessionList)
                {
                    // Clone session information for session object to be added in new track.
                    var sessionCopy = (Session)session.Clone(); 
                    var sessionRemainingTime = GetSessionInitialRemainingTime(session);
                    
                    sessionCopy.TalksList = new List<Talk>();

                    // Iterate Dictionary for sutaible talk
                    int index = 0;
                    while( sessionRemainingTime > 0 && index < this._durationTalkDictionary.Count)
                    {
                        var talk = this.GetSuitableTalk(sessionRemainingTime, ref index);
                        
                        // Include talk in Session and Remove its entry from dictionary
                        if (talk != null)
                        {
                            sessionCopy.TalksList.Add(talk);
                            sessionRemainingTime -= talk.Duration;
                            this.UpdateDurationTalkDictionary(talk.Duration);
                        }
                        else
                        {
                            // Break if even minimum talk duration is greater than session remaining time.
                            break;
                        }
                    }

                    if(Global.Valid(sessionCopy.TalksList))
                        newTrack.SessionList.Add(sessionCopy);

                    // Break if no talk left
                    if (this._durationTalkDictionary.Count == 0)
                        break;
                }

                if (!Global.Valid(newTrack.SessionList))
                {
                    logger.Error(Resources.ErrorMessageNoSutaibleTalkFound);
                    break;
                }

                conferenceSchedule.TrackList.Add(newTrack);

            } while (this._durationTalkDictionary.Count > 0);

            return conferenceSchedule;
        }

        /// <summary>
        /// Gets the suitable talk.
        /// </summary>
        /// <param name="sessionRemainingTime">The session remaining time.</param>
        /// <param name="index">The index.</param>
        /// <returns>The talk</returns>
        private Talk GetSuitableTalk(int sessionRemainingTime, ref int index)
        {
            Talk talk = null;

            if (this.ValidKeyValuePair(sessionRemainingTime))
            {
                talk = this._durationTalkDictionary[sessionRemainingTime][0];
            }
            else if (this.CanIncludedInSession(this._durationTalkDictionary.Keys.Max(), sessionRemainingTime))
            {
                talk = this._durationTalkDictionary[this._durationTalkDictionary.Keys.Max()][0];
            }
            else if (CanIncludedInSession(this._durationTalkDictionary.ElementAt(index).Key, sessionRemainingTime))
            {
                talk = this._durationTalkDictionary.ElementAt(index).Value[0];

                // Increment only if this talk is not the last value in this key
                if (_durationTalkDictionary.ElementAt(index).Value.Count > 1)
                    index++;
            }
            else if (this.CanIncludedInSession(this._durationTalkDictionary.Keys.Min(), sessionRemainingTime))
            {
                talk = this._durationTalkDictionary[this._durationTalkDictionary.Keys.Min()][0];
            }

            return talk;
        }

        /// <summary>
        /// Gets the session initial remaining time.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns>Remaining Time</returns>
        private int GetSessionInitialRemainingTime(Session session)
        {
            var duration = session.EndTime.Subtract(session.StartTime);

            if ((ConferenceConfig.GetConfigInstance()).TalkDurationUnit.IndexOf(Resources.TalkDurationUnitHr,
                StringComparison.OrdinalIgnoreCase) >= 0)
                return Convert.ToInt16(duration.TotalHours);

            return Convert.ToInt16(duration.TotalMinutes);
        }

        /// <summary>
        /// Valids the sessions list.
        /// </summary>
        /// <returns>The Boolean Status</returns>
        private bool ValidSessionsList()
        {
            return ConferenceConfig.GetConfigInstance().SessionsList != null && ConferenceConfig.GetConfigInstance().SessionsList.Count > 0;
        }

        /// <summary>
        /// Valids the key value pair.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The Boolean Status</returns>
        private bool ValidKeyValuePair(int key)
        {
            return this._durationTalkDictionary.ContainsKey(key) && this._durationTalkDictionary[key].Count > 0;
        }

        /// <summary>
        /// Determines whether this instance [can included in session] the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="sessionRemainingTime">The session remaining time.</param>
        /// <returns>The boolean status</returns>
        private bool CanIncludedInSession(int key, int sessionRemainingTime)
        {
            return this.ValidKeyValuePair(key) && key <= sessionRemainingTime && key > 0;
        }

        /// <summary>
        /// Updates the duration talk dictionary.
        /// </summary>
        /// <param name="key">The key.</param>
        private void UpdateDurationTalkDictionary(int key)
        {
            _durationTalkDictionary[key].RemoveAt(0);

            // Remove the key if all its talk values are shifted to scheduled track.
            if (_durationTalkDictionary[key].Count == 0)
               _durationTalkDictionary.Remove(key);
        }

        /// <summary>
        /// Populates the duration talk dictionary.
        /// </summary>
        /// <param name="talkList">The talk list.</param>
        private void PopulateDurationTalkDictionary(ICollection<Talk> talkList)
        {
            foreach (var talk in talkList)
            {
                if (!_durationTalkDictionary.ContainsKey(talk.Duration))
                {
                    _durationTalkDictionary.Add(talk.Duration, new List<Talk>());
                }
                _durationTalkDictionary[talk.Duration].Add(talk);
            }
        }
    }
}
