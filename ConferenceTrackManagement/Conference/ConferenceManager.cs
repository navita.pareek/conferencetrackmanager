﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: ConferenceManager.cs
// Description: 1. Singleton Class to Manage the Conference Scheduling. 
//              2. Exposes method to fetch the complete schedule.
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using System.Collections.Generic;
    using Properties;
    using NLog;

    /// <summary>
    /// The Singleton Conference Manager
    /// </summary>
    public class ConferenceManager
    {
        /// <summary>
        /// The track manager
        /// </summary>
        private static ConferenceManager _trackManager;

        /// <summary>
        /// The conference details
        /// </summary>
        private ConferenceSchedule ConferenceScheduleDetails { get; set; }

        /// <summary>
        /// Is configuration valid
        /// </summary>
        private bool _isConfigValid;

        /// <summary>
        /// The logger
        /// </summary>
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Prevents a default instance of the <see cref="ConferenceManager"/> class from being created.
        /// </summary>
        private ConferenceManager()
        {
            this.InitializeConferenceConfig();
            this.ValidateConferenceConfig();
        }

        /// <summary>
        /// Gets the conference manager.
        /// </summary>
        /// <returns>The Conference Manager Object</returns>
        public static ConferenceManager GetConferenceManager()
        {
            if (_trackManager == null)
                _trackManager = new ConferenceManager();

            return _trackManager;
        }

        /// <summary>
        /// Gets the conference details.
        /// </summary>
        /// <returns>The Conference Schedule</returns>
        public ConferenceSchedule GetConferenceDetails()
        {
            // Schedule Tracks
            this.ScheduleTracks();

            return this.ConferenceScheduleDetails;
        }

        /// <summary>
        /// Schedules the tracks if configurations valid.
        /// </summary>
        public void ScheduleTracks()
        {
            if (ScheduleTracksUtil())
            {
                logger.Info(Resources.MessageScheduleSuccess);
            }
            else
            {
                logger.Info(Resources.ErrorMessageScheduleFailure);
            }

        }

        /// <summary>
        /// Schedules the tracks utility.
        /// </summary>
        /// <returns></returns>
        private bool ScheduleTracksUtil()
        {
            this.displayConfigDetails();

            if (!this._isConfigValid)
            {
                logger.Error(Resources.ErrorMessageConfigInvalid);
                return false;
            }

            // Accepts input list
            var input = this.ReadInput();

            if (!Global.Valid(input))
            {
                logger.Error(Resources.ErrorMessageInputInvalid);
                return false;
            }

            logger.Debug(Resources.MessageInputValid);

            // Converts to talk list for scheduling
            var talkList = this.ConvertToTalkCollection(input);

            if (!Global.Valid(talkList))
            {
                logger.Error(Resources.ErrorMessageTalkListConversionFailed);
                return false;
            }

            logger.Debug(Resources.MessageInputConversion);

            // Get the tracks schedule
            this.ConferenceScheduleDetails = (new ConferenceScheduleGenerator()).GenerateConferenceSchedule(talkList);

            if (this.ConferenceScheduleDetails == null || !Global.Valid(this.ConferenceScheduleDetails.TrackList))
            {
                logger.Error(Resources.ErrorMessageManageTracksFailed);
                this.WriteOutput(Resources.ErrorMessageManageTracksFailed);
                return false;
            }

            logger.Debug(Resources.MessageTracksCreated);

            // Writes the output
            this.WriteOutput(ConferenceScheduleDetails.ToString());
            return true;
        }

        /// <summary>
        /// Displays the configuration details in case use not provided the configuration file.
        /// </summary>
        private void displayConfigDetails()
        {
            logger.Info(ConferenceConfig.GetConfigInstance().ToString());
        }

        /// <summary>
        /// Validates the conference configuration 
        /// and set boolean to represent if Configurations are valid.
        /// </summary>
        private void ValidateConferenceConfig()
        {
            var conferenceConfigValidator = new ConferenceConfigValidator();
            this._isConfigValid = conferenceConfigValidator.ValidateConfig();
        }

        /// <summary>
        /// Initializes the conference configuration.
        /// </summary>
        private void InitializeConferenceConfig()
        {
            try
            {
                var conferenceConfigUpdater = ConfigReaderFactory.GetFactoryInstance().GetInputCollectorInstance();
                conferenceConfigUpdater.ReadConferenceConfig();
            }
            catch (NotSupportedException)
            {
                logger.Error(Resources.ErrorMessageConfigInvalid);
            }
        }

        /// <summary>
        /// Writes the output through factory provided writer.
        /// </summary>
        /// <param name="outputToWrite">The Output.</param>
        private void WriteOutput(string outputToWrite)
        {
            var writer = OutputWriterFactory.GetFactoryInstance().GetOutputWriterInstance();
            writer.WriteOutput(outputToWrite);
        }

        /// <summary>
        /// Converts to talk collection.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The Collection of Talk</returns>
        private ICollection<Talk> ConvertToTalkCollection(string[] input)
        {
            return (new TalkListPopulator()).PopulateTalkListFromStringArray(input);
        }

        /// <summary>
        /// Gets the input through factory provided input collector.
        /// </summary>
        /// <returns>The array to talks</returns>
        private string[] ReadInput()
        {
            var inputCollector = InputReaderFactory.GetFactoryInstance().GetInputReaderInstance();
            return inputCollector.ReadInput();
        }
    }
}
