﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: ConferenceSchedule.cs
// Description: 1. Contains Collection of scheduled tracks. 
//              2. Provide function to convert collection to string.
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// The Conference Schedule 
    /// </summary>
    public class ConferenceSchedule
    {
        /// <summary>
        /// Gets or sets the track list.
        /// </summary>
        /// <value>
        /// The track list.
        /// </value>
        public ICollection<Track> TrackList { get; set; }

        /// <summary>
        /// Returns a Concatenated String which carries information of tracks.
        /// </summary>
        /// <returns>
        /// The Resulted String.
        /// </returns>
        public override string ToString()
        {
            var resultStringbuilder = new StringBuilder();
            if (this.TrackList != null && this.TrackList.Count > 0)
            {
                // Iterate over each track in the list and convert to string.
                for (int index = 0; index < this.TrackList.Count; index++)
                {
                    resultStringbuilder.Append(this.TrackList.ElementAt(index).ToString());
                    resultStringbuilder.Append(System.Environment.NewLine);
                }
            }

            return resultStringbuilder.ToString();
        }
    }
}
