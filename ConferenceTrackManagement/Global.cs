﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: Global.cs
// Description: 1. Abstract file to provide global functions and variables.
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System.Collections.Generic;
    using ConferenceTrackManagement.Properties;

    /// <summary>
    /// The Global Methods and Variables
    /// </summary>
    public abstract class Global
    {
        /// <summary>
        /// Gets or sets the configuration filename.
        /// </summary>
        /// <value>
        /// The configuration filename.
        /// </value>
        public static string ConfigFilename { get; set; } = Resources.DefaultXmlConfigFileName;

        /// <summary>
        /// Valids the specified object collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectCollection">The object collection.</param>
        /// <returns>The Boolean Status</returns>
        public static bool Valid<T>(ICollection<T> objectCollection)
        {
            return (objectCollection != null && objectCollection.Count > 0);
        }
    }
}
