﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: Talk.cs
// Description: 1. Provide functions to clone and convert Session information to string.
//------------------------------------------------------------------------------

namespace ConferenceTrackManagement
{
    using System;
    using System.Text;

    /// <summary>
    /// The Talk presented in Session
    /// </summary>
    public class Talk
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        public int Duration { get; set; }

        /// <summary>
        /// Returns a String of talk properties.
        /// </summary>
        /// <returns>
        /// The Resulted String.
        /// </returns>
        public string ToString(ref DateTime nextTalkStartTime)
        {
            var resultStringbuilder = new StringBuilder();

            resultStringbuilder.Append(nextTalkStartTime.ToString("h:mmtt"));
            resultStringbuilder.Append(" ");
            resultStringbuilder.Append(this.Description);
            resultStringbuilder.Append(" ");
            resultStringbuilder.Append(this.Duration);
            resultStringbuilder.Append(ConferenceConfig.GetConfigInstance().TalkDurationUnit);

            nextTalkStartTime = nextTalkStartTime.AddMinutes(this.Duration);

            return resultStringbuilder.ToString();
        }
    }
}
