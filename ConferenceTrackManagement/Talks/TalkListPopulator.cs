﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: TalkListPopulator.cs
// Description: 1. Populates talk list from the string array.
//------------------------------------------------------------------------------

using System.Linq;

namespace ConferenceTrackManagement
{
    using System;
    using System.Collections.Generic;
    using Properties;
    using NLog;

    /// <summary>
    /// Populates the talk list
    /// </summary>
    public class TalkListPopulator
    {
        /// <summary>
        /// The logger
        /// </summary>
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Populates the talk list from string array.
        /// </summary>
        /// <param name="stringArray">The string list.</param>
        /// <returns>The talk list</returns>
        public ICollection<Talk> PopulateTalkListFromStringArray(string[] stringArray)
        {
            ICollection<Talk> inputList = null;

            if (Global.Valid(stringArray))
            {
                inputList = new List<Talk>();

                foreach (var talk in stringArray)
                {
                    // Split string considering last substring is duration detail
                    string talkDuration = string.Empty;
                    string talkDescription = string.Empty;

                    bool isDurationProvided = GetDescriptionDurationSeperated(talk, ref talkDescription, ref talkDuration);

                    if (!isDurationProvided)
                    {
                        if (talk.IndexOf(Resources.LightningTalkDescription, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            talkDescription = talk;
                            talkDuration = ConferenceConfig.GetConfigInstance().LightningTalkDuration;
                        }
                        else
                        {
                            // If no duration provided, add default duration
                            logger.Debug(Resources.LogMessageUsingDefaultValue + "-" + talk + " " + ConferenceConfig.GetConfigInstance().DefaultDuration);
                            talkDescription = talk;
                            talkDuration = ConferenceConfig.GetConfigInstance().DefaultDuration;
                        }
                    }

                    // Add to talk list
                    if (ConferenceConfig.GetConfigInstance().AllowDuplicateTalks || inputList.All(x => x.Description != talkDescription))
                    {
                        inputList.Add(new Talk
                        {
                            Description = talkDescription,
                            Duration = int.Parse(talkDuration)
                        });
                    }
                    else
                    {
                        logger.Info(Resources.ErrorMessageDuplicateTalk + "-" + talk);
                    }
                }

                logger.Debug(Resources.MessageTalkListCount +" " + inputList.Count);
            }
            else
            {
                logger.Error(Resources.ErrorMessageInputInvalid);
            }
            
            return inputList;
        }

        private bool GetDescriptionDurationSeperated(string talk, ref string talkDescription, ref string talkDuration)
        {
            string[] talkdetails = talk.Split(' ');

            // Consider Duration provided at end with unit present
            talkDuration = talkdetails[talkdetails.Length - 1];
            if (isDuration(talkDuration))
            {
                updateDurationDescription(talk, ref talkDescription, ref talkDuration);
                return true;
            }

            // Consider Duration provided at start with unit present
            talkDuration = talkdetails[0];
            if (isDuration(talkDuration))
            {
                updateDurationDescription(talk, ref talkDescription, ref talkDuration);
                return true;
            }

            // Consider Duration provided at end without unit
            talkDuration = talkdetails[talkdetails.Length - 1];
            if (isPositiveInteger(talkDuration))
            {
                updateDurationDescription(talk, ref talkDescription, ref talkDuration);
                return true;
            }

            // Consider Duration provided at start without unit
            talkDuration = talkdetails[0];
            if (isPositiveInteger(talkDuration))
            {
                updateDurationDescription(talk, ref talkDescription, ref talkDuration);
                return true;
            }

            // Duration not provided
            return false;
        }

        private void updateDurationDescription(string talk, ref string talkDescription, ref string talkDuration)
        {
            talkDescription = talk.Replace(talkDuration, string.Empty).Trim();
            talkDuration = talkDuration.ToLower().Replace(ConferenceConfig.GetConfigInstance().TalkDurationUnit.ToLower(),
                string.Empty);
        }

        private bool isDuration(string duration)
        {
            if (duration.IndexOf(ConferenceConfig.GetConfigInstance().TalkDurationUnit,
                       StringComparison.OrdinalIgnoreCase) >= 0)
            {
                duration = duration.ToLower().Replace(ConferenceConfig.GetConfigInstance().TalkDurationUnit.ToLower(),
                    string.Empty);

                return (isPositiveInteger(duration));
            }

            return false;
        }

        private bool isPositiveInteger(string duration)
        {
            int tryParseResult;
            return (int.TryParse(duration, out tryParseResult) && int.Parse(duration) > 0);
        }
    }
}
