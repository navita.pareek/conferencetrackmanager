﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagement
// Input: List of talks the with descrption and duration.
// Output: Scheduled time plan of the conference.
// Configuration: System configured through ConferenceConfig.xml. 
//                Check UserManual for further information.
// File: Application.cs
// Description: Entry point to use Conference track management.
//------------------------------------------------------------------------------

using System;

namespace ConferenceTrackManagement
{
    /// <summary>
    /// Driver Application
    /// </summary>
    public class Application
    {
        /// <summary>
        /// Main Entry Point.
        /// </summary>
        /// <param name="args">The argument list.</param>
        public static void Main(string[] args)
        {
            // Get configuration filename from argument if provided
            // else use default "ConferenceConfig.xml"
            if (args.Length > 0)
            {
                Global.ConfigFilename = args[0];
            }

            var conferenceManager = ConferenceManager.GetConferenceManager();
            conferenceManager.ScheduleTracks();
        }

    }
}
