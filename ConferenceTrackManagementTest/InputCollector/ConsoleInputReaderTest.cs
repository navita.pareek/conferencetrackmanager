﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagementTest
// Description: Test different components of ConferenceTrackManagement Project.
// File: ConsoleInputReaderTest.cs
//------------------------------------------------------------------------------

namespace ConferenceTrackManagementTest
{
    using ConferenceTrackManagement;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ConsoleInputReaderTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();
        }

        [TestMethod]
        public void ConsoleInputCollector_getInput()
        {
            var inputCollector = new ConsoleInputReader();
            var result = inputCollector.ReadInput();

            Assert.IsNotNull(result);
        }
    }
}
