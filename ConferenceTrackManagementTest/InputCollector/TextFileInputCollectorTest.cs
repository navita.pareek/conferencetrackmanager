﻿using System;
using ConferenceTrackManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConferenceTrackManagementTest
{
    [TestClass]
    public class TextFileInputCollectorTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();
        }

        [TestMethod]
        public void TextFileInputCollector_getTalkDetails()
        {
            var inputCollector = new TextFileInputReader();
            var result = inputCollector.ReadInput();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Length > 0);

        }
    }
}
