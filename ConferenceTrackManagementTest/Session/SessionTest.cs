﻿using System;
using System.Collections;
using System.Collections.Generic;
using ConferenceTrackManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConferenceTrackManagementTest
{
    [TestClass]
    public class SessionTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();
        }

        [TestMethod]
        public void Session_ToString()
        {
            var session = getSession();

            var result = session.ToString();

            Assert.IsTrue(!string.IsNullOrWhiteSpace(result));
        }

        private Session getSession()
        {
            var session = new Session
            {
                StartTime = Convert.ToDateTime("09:00AM"),
                EndTime = Convert.ToDateTime("12:00PM"),
                TalksList = getTalkList(2)
            };

            return session;
        }

        private ICollection<Talk> getTalkList(int talksCount = 4)
        {
            var talkList = new List<Talk>();

            for (int count = 1; count <= talksCount; count++)
            {
                talkList.Add(
                new Talk
                {
                    Description = "XYZ",
                    Duration = 15 * count
                });
            }

            return talkList;
        }
    }
}
