﻿using System;
using System.Collections;
using System.Collections.Generic;
using ConferenceTrackManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConferenceTrackManagementTest
{
    [TestClass]
    public class TalkTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();
        }

        [TestMethod]
        public void Talk_ToString()
        {
            var talk = getTalk();
            var time = DateTime.Now;
            var result = talk.ToString(ref time);

            Assert.IsTrue(!string.IsNullOrWhiteSpace(result));
        }

        private Talk getTalk()
        {
            var talk = new Talk
            {
                Description = "XYZ",
                Duration = 20
            };

            return talk;
        }
    }
}
