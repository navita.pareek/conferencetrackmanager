﻿using System;
using ConferenceTrackManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConferenceTrackManagementTest
{
    [TestClass]
    public class TalkListPopulatorTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();
        }

        [TestMethod]
        public void TalkListPopulator_PopulatorTalkListFromStringArray()
        {
            string[] inputList = {"Code Complete Defination 15min", "Style Cop Lightning", "Free Style Coding"};

            var talkListPopulator = new TalkListPopulator();
            var result = talkListPopulator.PopulateTalkListFromStringArray(inputList);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count == 3);
        }
    }
}
