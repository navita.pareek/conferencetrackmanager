﻿using System;
using System.Collections;
using System.Collections.Generic;
using ConferenceTrackManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConferenceTrackManagementTest
{
    [TestClass]
    public class TrackTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();
        }

        [TestMethod]
        public void Track_ToString()
        {
            var track = new Track
            {
                SessionList = getSessionList(),
                TrackNumber = 1
            };

            var result = track.ToString();

            Assert.IsTrue(!string.IsNullOrWhiteSpace(result));
        }

        private ICollection<Session> getSessionList(int sessionCount = 1)
        {
            var sessionList = new List<Session>();

            for (int count = 1; count <= sessionCount; count++)
            {
                sessionList.Add(new Session
                {
                    StartTime = DateTime.Now.AddHours(count),
                    EndTime = DateTime.Now.AddHours(count + 1),
                    TalksList = getTalkList(2)
                });
            }

            return sessionList;
        }

        private ICollection<Talk> getTalkList(int talksCount = 4)
        {
            var talkList = new List<Talk>();

            for (int count = 1; count <= talksCount; count++)
            {
                talkList.Add(
                new Talk
                {
                    Description = "XYZ",
                    Duration = 15 * count
                });
            }

            return talkList;
        }
    }
}
