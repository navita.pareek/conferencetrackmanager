﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ConferenceTrackManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConferenceTrackManagementTest
{
    [TestClass]
    public class ConferenceScheduleGeneratorTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();
        }

        [TestMethod]
        public void ConferenceScheduleGenerator_GenerateConferenceSchedule_InvalidTalkList_ShouldFail()
        {
            ConferenceScheduleGenerator conferenceScheduleGenerator = new ConferenceScheduleGenerator();
            var result = conferenceScheduleGenerator.GenerateConferenceSchedule(null);

            Assert.IsNull(result);
        }

        [TestMethod]
        public void ConferenceScheduleGenerator_GenerateConferenceSchedule_ValidTalkList_ShouldSucceed()
        {
            var talkList = getTalkList();
            ConferenceScheduleGenerator conferenceScheduleGenerator = new ConferenceScheduleGenerator();
            var result = conferenceScheduleGenerator.GenerateConferenceSchedule(talkList);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.TrackList.Count > 0);
            Assert.IsTrue(result.TrackList.ElementAt(0).SessionList.Count > 0);
            Assert.IsTrue(result.TrackList.ElementAt(0).SessionList.ElementAt(0).TalksList.Count > 0);
        }

        [TestMethod]
        public void ConferenceScheduleGenerator_GenerateConferenceSchedule_ValidTalkList2_ShouldSucceed()
        {
            var talkList = getTalkList(8);
            ConferenceScheduleGenerator conferenceScheduleGenerator = new ConferenceScheduleGenerator();
            var result = conferenceScheduleGenerator.GenerateConferenceSchedule(talkList);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.TrackList.Count > 0);
            Assert.IsTrue(result.TrackList.ElementAt(0).SessionList.Count > 0);
            Assert.IsTrue(result.TrackList.ElementAt(0).SessionList.ElementAt(0).TalksList.Count > 0);
        }

        private ICollection<Talk> getTalkList(int talksCount = 4)
        {
            var talkList = new List<Talk>();

            for (int count = 1; count <= talksCount; count++)
            {
                talkList.Add(
                new Talk
                {
                    Description = "XYZ",
                    Duration = 15 * count
                });
            }

            return talkList;
        }
    }
}
