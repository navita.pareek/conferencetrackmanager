﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ConferenceTrackManagement;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConferenceTrackManagementTest
{
    [TestClass]
    public class ConferenceMangerTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();
        }

        [TestMethod]
        public void ConferenceManager_GetConferenceDetails()
        {
            var conferenceManager = ConferenceManager.GetConferenceManager();
            var result = conferenceManager.GetConferenceDetails();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.TrackList.Count > 0);
            Assert.IsTrue(result.TrackList.ElementAt(0).SessionList.Count > 0);
            Assert.IsTrue(result.TrackList.ElementAt(0).SessionList.ElementAt(0).TalksList.Count > 0);
        }
    }
}
