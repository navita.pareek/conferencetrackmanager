﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagementTest
// Description: Test different components of ConferenceTrackManagement Project.
// File: ConferenceConfigValidorTest.cs
//------------------------------------------------------------------------------

namespace ConferenceTrackManagementTest
{
    using ConferenceTrackManagement;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Configuration Validator Test
    /// Test Cases are Ignored as Configuration is Singleton and better be tested individually.
    /// </summary>
    [TestClass]
    public class ConferenceConfigValidorTest
    {
        [TestMethod]
        [Ignore]
        public void ConferenceConfigValidator_InvalidSessionTime_ShouldFail()
        {
            Global.ConfigFilename = "ConferenceConfigTest_Invalid1.xml";
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();

            var configValidor = new ConferenceConfigValidator();
            var result = configValidor.ValidateConfig();

            Assert.IsFalse(result);
        }

        [TestMethod]
        [Ignore]
        public void ConferenceConfigValidator_OverLappingSessionTime_ShouldFail()
        {
            Global.ConfigFilename = "ConferenceConfigTest_Invalid2.xml";
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();

            var configValidor = new ConferenceConfigValidator();
            var result = configValidor.ValidateConfig();

            Assert.IsFalse(result);
        }

        [TestMethod]
        [Ignore]
        public void ConferenceConfigValidator_ConfigReaderNotCalled_ShouldFail()
        {
            var configValidor = new ConferenceConfigValidator();
            var result = configValidor.ValidateConfig();

            Assert.IsFalse(result);
        }

    }
}
