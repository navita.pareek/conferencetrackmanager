﻿//-----------------------------------------------------------------------------
// Project: ConferenceTrackManagementTest
// Description: Test different components of ConferenceTrackManagement Project.
// File: XmlConfigReaderTest.cs
//------------------------------------------------------------------------------

namespace ConferenceTrackManagementTest
{
    using ConferenceTrackManagement;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Test Conference XML Configuration Reader
    /// Test Cases are Ignored as Configuration is Singleton and better be tested individually.
    /// </summary>
    [TestClass]
    public class XmlConfigReaderTest
    {
        [TestMethod]
        [Ignore] 
        public void XmlConfigReader_PopulateConferenceConfig_InvalidConfigurationFile_ShouldFail()
        {
            Global.ConfigFilename = "XYZ.txt";
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();

            Assert.IsNull(ConferenceConfig.GetConfigInstance().SessionsList);
        }

        [TestMethod]
        [Ignore]
        public void XmlConfigReader_PopulateConferenceConfig_OnlyWithRequiredAttributes_ShouldSucceed()
        {
            Global.ConfigFilename = "ConferenceConfigTest_Valid.xml";
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();

            Assert.AreNotEqual(0, ConferenceConfig.GetConfigInstance().SessionsList.Count);
        }

        [TestMethod]
        [Ignore]
        public void XmlConfigReader_PopulateConferenceConfig_OnlyWithRequiredAttributesAbsent_ShouldFail()
        {
            Global.ConfigFilename = "ConferenceConfigTest_Invalid.xml";
            var configUpdater = new XmlConfigReader();
            configUpdater.ReadConferenceConfig();

            Assert.IsNull(ConferenceConfig.GetConfigInstance().SessionsList);
        }
    }
}
